# MiToken
**mitoken** is a shell script that extract the all important token of the Xiaomi Roborock S50 vacuum cleaner.
This token is useful if you want to use various apps to controol the robot

## Usage
```
root@machine:~/roborock/mitoken# ./mitoken.sh -h
Usage: ./mitoken.sh [-h] [-v] [-d roborock_ip] [-p roborock_port] [-i networkd_interface]

-v: add verbosity
-h: show this help
-d <aa.bb.cc.dd>: roborock ip address (default: 192.168.8.1)
-p <port_number>: roborock port (default: 54321)
-i <interface_name>: network interface (wireless lan interface)
```

## Dependencies

The script uses netcat to send udp datagram to the device and extract the token from the answered data

## Thanks

Huge thanks to this page for forging the datagram :
[jghaanstrai](http://github.com/jghaanstra/com.xiaomi-miio/blob/master/docs/obtain_token.md)
