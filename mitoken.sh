#!/bin/bash

ROBOROCK_ITF="wlxa0f3c12a75cf"
ROBOROCK_IP="192.168.8.1"
ROBOROCK_PORT=54321
ROBOROCK_PKT='\x21\x31\x00\x20\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff'

help() {
	usage
	echo ""
	echo "-v: add verbosity"
	echo "-h: show this help"
	echo "-d <aa.bb.cc.dd>: roborock ip address (default: 192.168.8.1)"
	echo "-p <port_number>: roborock port (default: 54321)"
	echo "-i <interface_name>: network interface (wireless lan interface)"
	echo ""
	exit 1
}

usage() {
	echo "Usage: $0 [-h] [-v] [-d roborock_ip] [-p roborock_port] [-i networkd_interface]"
}

if [ "$EUID" -ne 0 ]
then
	echo "$0 needs root privileges"
	exit
fi

debug=0

while getopts "hvd:p:i:" option
do
	case $option in
		v)
			debug=1
			;;
		d)
			ROBOROCK_IP=$OPTARG
			;;
		p)
			ROBOROCK_PORT=$OPTARG
			;;
		i)
			ROBOROCK_ITF=$OPTARG
			;;
		h)
			help
			exit 2
			;;
		*)
			usage
			exit 1
			;;
	esac
done

if [ $debug -eq 1 ]
then
	set -x
fi

echo "Trying to fetch token from IP=$ROBOROCK_IP PORT=$ROBOROCK_PORT IFACE=$ROBOROCK_ITF"

#send command to roborock
token=$( echo -ne '\x21\x31\x00\x20\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff' | netcat -vv -W 1 -q 3 -n -u $ROBOROCK_IP $ROBOROCK_PORT | od --endian=big -x -w16 -j16 | head -1 | cut -d " " -f2- |tr -d " ")

echo -e "\nYour roborock token is: $token\n"


